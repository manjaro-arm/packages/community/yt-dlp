# Maintainer: Dan Johansen <strit@manjaro.org>

## Arch maintainers:
# Maintainer: Antonio Rojas <arojas@archlinux.org>
# Contributor: Stefan Tatschner <stefan@rumpelsepp.org>
# Contributor: katt <magunasu.b97@gmail.com>

pkgname=yt-dlp
pkgver=2022.01.21
pkgrel=1
pkgdesc='A youtube-dl fork with additional features and fixes'
arch=(any)
url='https://github.com/yt-dlp/yt-dlp'
license=(Unlicense)
depends=(python-mutagen python-pycryptodomex python-websockets)
makedepends=(python-setuptools)
optdepends=('ffmpeg: for video post-processing'
            'rtmpdump: for rtmp streams support'
            'atomicparsley: for embedding thumbnails into m4a files'
            'aria2: for using aria2 as external downloader')
source=(https://github.com/yt-dlp/yt-dlp/archive/$pkgver/$pkgname-$pkgver.tar.gz)
sha256sums=('f3435b9e8c1b5666e03a97da23d86c277cbbcb9fd5a7a2f2ca086488b00735c1')

build() {
  cd $pkgname-$pkgver
  python setup.py build
}

package() {
  cd $pkgname-$pkgver
  python setup.py install --root="$pkgdir" --skip-build --optimize=1
}
